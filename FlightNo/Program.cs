﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightNo.Model;
class Program {

    static void Main (string[] args) {

        var flight = new Flight { FlightNumber_No = "BR123" };
        var mainPilot = new FlightCrew { Name = "Rich", FlightCrewType = CrewType.Pilot };
        var subPilot = new FlightCrew { Name = "SubRich", FlightCrewType = CrewType.Pilot };

        var Ca_1 = new FlightCrew { Name = "CaRich_1", FlightCrewType = CrewType.CA };
        var Ca_2 = new FlightCrew { Name = "CaRich_2", FlightCrewType = CrewType.CA };
        var Ca_3 = new FlightCrew { Name = "CaRich_3", FlightCrewType = CrewType.CA };
        flight.MainPilot = mainPilot;
        flight.SubPilot = subPilot;

        flight.CrewList.Add (mainPilot);
        flight.CrewList.Add (subPilot);
        flight.CrewList.Add (Ca_1);
        flight.CrewList.Add (Ca_2);
        flight.CrewList.Add (Ca_3);

        System.Console.WriteLine (flight.FlightNumber_No + "機師是：" + flight.MainPilot.Name + "副機師是：" + flight.SubPilot.Name + "，共有CA人數" + flight.CALIST.Count () + "人");

    }
}