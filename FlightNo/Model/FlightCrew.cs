namespace FlightNo.Model {
    public class FlightCrew {
        public string Name { get; set; }
        public CrewType FlightCrewType { get; set; }
    }
}