using System.Collections.Generic;
using System.Linq;

namespace FlightNo.Model {
    public class Flight {
        public string FlightNumber_No { get; set; }

        public List<FlightCrew> CrewList { get; set; } = new List<FlightCrew> ();

        public FlightCrew MainPilot { get; set; }

        public FlightCrew SubPilot { get; set; }

        public List<FlightCrew> CALIST => CrewList.Where (x => x.FlightCrewType == CrewType.CA).ToList ();
    }

}